function initApp() {
    var googleEmail = "";
    var txtEmail = document.getElementById('inputEmail');
    var txtPassword = document.getElementById('inputPassword');
    var btnLogin = document.getElementById('btnLogin');
    var btnGoogle = document.getElementById('btngoogle');
    var btnSignUp = document.getElementById('btnSignUp');
    btnLogin.addEventListener('click', function() {
        firebase.auth().signInWithEmailAndPassword(txtEmail.value, txtPassword.value)
        .then(function(){
            window.location.href = "index.html";
        })
        .catch(function(error) {
            var errorMessage = error.message;
            create_alert("error", errorMessage);
        });
    });

    btnGoogle.addEventListener('click', function() {
        var provider = new firebase.auth.GoogleAuthProvider();
        firebase.auth().signInWithPopup(provider).then(function(e){
            if(e.additionalUserInfo.isNewUser){
                googleEmail = e.additionalUserInfo.profile.email;
                firebase.database().ref("user_list").push({email: googleEmail}).then(async (e)=>{
                    await firebase.database().ref("user_list/" + e.key + "/chatroom").push({
                        id:"-MYpHn_yC1oY_jsYBLY2",
                        name:"public"
                    });  
                    await firebase.database().ref("chatrooms/-MYpHn_yC1oY_jsYBLY2/user").push(
                        {
                            email: googleEmail,
                            user_id: e.key
                        }
                    )
                    window.location.href = "index.html";
                });
            }
            else{
                window.location.href = "index.html"
            }
        })
        .catch(function(error) {
            var errorMessage = error.message;
            create_alert("error", errorMessage);
        });
    });

    btnSignUp.addEventListener('click', function() {
        firebase.auth().createUserWithEmailAndPassword(txtEmail.value, txtPassword.value)
        .then(function(){
            firebase.database().ref("user_list").push({email: txtEmail.value}).then(async (e)=>{
                await firebase.database().ref("user_list/" + e.key + "/chatroom").push({
                    id:"-MYpHn_yC1oY_jsYBLY2",
                    name:"public"
                });
                await firebase.database().ref("chatrooms/-MYpHn_yC1oY_jsYBLY2/user").push(
                    {
                        email: txtEmail.value,
                        user_id: e.key
                    }
                )
                window.location.href = "index.html";
            });
        })
        .catch(function(error) {
            var errorMessage = error.message;
            create_alert("error", errorMessage);
        });
    });
}
function create_alert(type, message) {
    var alertarea = document.getElementById('custom-alert');
    if (type == "success") {
        str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Success! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    } else if (type == "error") {
        str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    }
}

window.onload = function() {
    initApp();
};