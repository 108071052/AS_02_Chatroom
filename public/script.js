var user_email = '';
var user_id = ''
var user_roomid_list = [];
var user_roomname_list = [];
var room_div = [];
var message_div = [];
var current_room_index = 0;
var member_div = [];
var show = false;
var now = new Date();
var Pre_update = now.getTime();
async function select_room(id){
    transition = document.getElementById('transition');
    if(current_room_index != user_roomid_list.indexOf(id)){
        transition.style.display = "block";
        document.getElementById('message-list').innerHTML = "";
        current_room_index = user_roomid_list.indexOf(id);
        document.getElementById('roomName').innerHTML = "當前房間名稱:" + user_roomname_list[current_room_index];
        message_div = [];
        member_div = [];
        var member_data = firebase.database().ref("chatrooms/" + id + "/user");
        await member_data.once('value').then(function(snapshot){
            snapshot.forEach(function(childSnapshot){
                member_data = childSnapshot.val();
                var member_email = member_data.email;
                var div_member = "<div class=\"member-data\">" + member_email + "</div>\n";
                member_div.push(div_member);
            })
        })
        if(show){
            document.getElementById('member-list').innerHTML = member_div.join("");
        }
        var message_data = firebase.database().ref("chatrooms/" + id + "/message_list");
        await message_data.once('value').then(function(snapshot){
            snapshot.forEach(function(childSnapshot){
                message_data = childSnapshot.val();
                var content = message_data.content;
                content = content.replace(/</g, "&lt;").replace(/>/g, "&gt;");
                var user = message_data.user_email;
                var time = new Date(message_data.date);
                var date = 
                time.getFullYear()+'年'+
                (time.getMonth()+1)+'月'+
                time.getDate()+'日('+
                time.getHours()+':'+time.getMinutes()+
                ')';
                var div_message = "";
                if(user == user_email){
                    div_message = "<div class=\"mymessage-data\">\n<div class=\"date\">" + date + "</div>\n" + "<div class=\"mycontent\">" + content + "</div>\n" + "</div>";
                }
                else{
                    div_message = "<div class=\"othersmessage-data\">\n<div class=\"date\">" + date + "</div>\n" + "<div class=\"username\">" + user + "</div>\n" + "<div class=\"otherscontent\">" + content + "</div>\n" + "</div>";
                }
                message_div.push(div_message);
            })
        })
        document.getElementById('message-list').innerHTML = message_div.join('');
        document.getElementById('message-list').scrollTop = document.getElementById('message-list').scrollHeight;
        transition.style.display = "none";
    }
}

window.onload = async function() {
    if (!('Notification' in window)) {
    console.log('This browser does not support notification');
    }
    if (Notification.permission === 'default' || Notification.permission === 'undefined') {
        Notification.requestPermission();
    }
    firebase.auth().onAuthStateChanged(async function(user) {
        var menu = document.getElementById('dynamic-menu');
        transition = document.getElementById('transition');
        if (user) {
            transition.style.display = "block";
            document.getElementById('roomName').innerHTML = "當前房間名稱:public";
            create_room_btn = document.querySelector('#create-room');
            create_room_btn.className = "nav-link";
            invite_btn = document.querySelector('#invite-btn');
            invite_btn.className = "nav-link";
            user_email = user.email;
            user_list = firebase.database().ref('user_list');
            await user_list.once('value').then(function(snapshot){
                snapshot.forEach(function(childSnapshot){
                    var childData = childSnapshot.val();
                    if(childData.email == user_email){
                        user_id = childSnapshot.key;
                        room = firebase.database().ref('user_list/' + childSnapshot.key + '/chatroom');
                        room.once('value').then(async function(snapshot){
                            await snapshot.forEach(function(childSnapshot){
                                var roomkey = childSnapshot.val();
                                user_roomname_list.push(roomkey.name);
                                user_roomid_list.push(roomkey.id);
                            })
                            for(i=0;i<user_roomid_list.length;i++){
                                message_addr = firebase.database().ref('chatrooms/' + user_roomid_list[i] + '/message_list');
                                message_addr.on('child_added', function(e){
                                    room_id = e.ref.parent.parent.key;
                                    message_data = e.val();
                                    if(room_id == user_roomid_list[current_room_index]){
                                        var content = message_data.content;
                                        content = content.replace(/</g, "&lt;").replace(/>/g, "&gt;");
                                        var user = message_data.user_email;
                                        if(user_email != user){
                                            var time = new Date(message_data.date);
                                            var date = 
                                            time.getFullYear()+'年'+
                                            (time.getMonth()+1)+'月'+
                                            time.getDate()+'日('+
                                            time.getHours()+':'+time.getMinutes()+
                                            ')';
                                            var div_message = "";
                                            div_message = "<div class=\"othersmessage-data\">\n<div class=\"date\">" + date + "</div>\n" + "<div class=\"username\">" + user + "</div>\n" + "<div class=\"otherscontent\">" + content + "</div>\n" + "</div>";
                                            message_div.push(div_message);
                                            document.getElementById('message-list').innerHTML = message_div.join('');
                                            document.getElementById('message-list').scrollTop = document.getElementById('message-list').scrollHeight;   
                                        }
                                    }
                                    else{
                                        if(message_data.date > Pre_update){
                                            var notify = new Notification( message_data.user_email + "傳送資訊到:" + user_roomname_list[user_roomid_list.indexOf(room_id)], {
                                                body: message_data.content
                                            });
                                            Pre_update = message_data.date;
                                            notify.onclick = function(e) {
                                                e.preventDefault();
                                                select_room(room_id);
                                            }
                                        }                                     
                                    }
                                })
                            }
                        }).then(async ()=>{
                            var room = firebase.database().ref("user_list/" + user_id + "/chatroom");
                            console.log(room);
                            room.on('child_added', function(e){
                                room_data = e.val();
                                if(user_roomname_list.indexOf(room_data.name) == -1){
                                    user_roomname_list.push(room_data.name);
                                    user_roomid_list.push(room_data.id);
                                    var div_room = "<div id=\"" + user_roomid_list[user_roomid_list.length-1] + "\" onclick=\"select_room(this.id)\"" + " class=\"room-name\">" + user_roomname_list[user_roomid_list.length-1] + "</div>\n"
                                    room_div.push(div_room);
                                    document.getElementById('Chatroom-bar').innerHTML = room_div.join('');
                                    var notify = new Notification("您被邀請進入:", {
                                        body: "房間名稱：" + room_data.name
                                    });
                                    Pre_update = message_data.date;
                                    notify.onclick = function(e) {
                                        e.preventDefault();
                                        select_room(room_data.id);
                                    }
                                    message_addr = firebase.database().ref('chatrooms/' + room_data.id + '/message_list');
                                    message_addr.on('child_added', function(e){
                                        room_id = e.ref.parent.parent.key;
                                        message_data = e.val();
                                        if(room_id == user_roomid_list[current_room_index]){
                                            var content = message_data.content;
                                            content = content.replace(/</g, "&lt;").replace(/>/g, "&gt;");
                                            var user = message_data.user_email;
                                            if(user_email != user){
                                                var time = new Date(message_data.date);
                                                var date = 
                                                time.getFullYear()+'年'+
                                                (time.getMonth()+1)+'月'+
                                                time.getDate()+'日('+
                                                time.getHours()+':'+time.getMinutes()+
                                                ')';
                                                var div_message = "";
                                                div_message = "<div class=\"othersmessage-data\">\n<div class=\"date\">" + date + "</div>\n" + "<div class=\"username\">" + user + "</div>\n" + "<div class=\"otherscontent\">" + content + "</div>\n" + "</div>";
                                                message_div.push(div_message);
                                                document.getElementById('message-list').innerHTML = message_div.join('');
                                                document.getElementById('message-list').scrollTop = document.getElementById('message-list').scrollHeight;   
                                            }
                                        }
                                        else{
                                            if(message_data.date > Pre_update){
                                                var notify = new Notification( message_data.user_email + "傳送資訊到:" + user_roomname_list[user_roomid_list.indexOf(room_id)], {
                                                    body: message_data.content
                                                });
                                                Pre_update = message_data.date;
                                                notify.onclick = function(e) {
                                                    e.preventDefault();
                                                    select_room(room_id);
                                                }
                                            }                                     
                                        }
                                    })
                                }
                            })
                            var member_data = firebase.database().ref("chatrooms/" + user_roomid_list[0] + "/user");
                            await member_data.once('value').then(function(snapshot){
                                snapshot.forEach(function(childSnapshot){
                                    member_data = childSnapshot.val();
                                    var member_email = member_data.email;
                                    var div_member = "<div class=\"member-data\">" + member_email + "</div>\n";
                                    member_div.push(div_member);
                                })
                            })
                            public_ref = firebase.database().ref('chatrooms/' + user_roomid_list[0] + "/message_list");
                            await public_ref.once('value').then(function(snapshot){
                                snapshot.forEach(function(childSnapshot){
                                    message_data = childSnapshot.val();
                                    var content = message_data.content;
                                    content = content.replace(/</g, "&lt;").replace(/>/g, "&gt;");
                                    var user = message_data.user_email;
                                    var time = new Date(message_data.date);
                                    var date = 
                                    time.getFullYear()+'年'+
                                    (time.getMonth()+1)+'月'+
                                    time.getDate()+'日('+
                                    time.getHours()+':'+time.getMinutes()+
                                    ')';
                                    if(user == user_email){
                                        div_message = "<div class=\"mymessage-data\">\n<div class=\"date\">" + date + "</div>\n" + "<div class=\"mycontent\">" + content + "</div>\n" + "</div>";
                                    }
                                    else{
                                        div_message = "<div class=\"othersmessage-data\">\n<div class=\"date\">" + date + "</div>\n" + "<div class=\"username\">" + user + "</div>\n" + "<div class=\"otherscontent\">" + content + "</div>\n" + "</div>";
                                    }
                                    message_div.push(div_message);
                                })
                            })
                            document.getElementById('message-list').innerHTML = message_div.join('');
                            document.getElementById('message-list').scrollTop = document.getElementById('message-list').scrollHeight;
                            transition = document.getElementById('transition');
                            transition.style.display = "none";
                            for(i=0;i<user_roomid_list.length;i++){
                                var div_room = "<div id=\"" + user_roomid_list[i] + "\" onclick=\"select_room(this.id)\"" + " class=\"room-name\">" + user_roomname_list[i] + "</div>\n"
                                room_div.push(div_room);
                            }
                            document.getElementById('Chatroom-bar').innerHTML = room_div.join('');                          
                        })
                    }
                })
            })
            menu.innerHTML = "<span class='dropdown-item'>" + user.email + "</span><span class='dropdown-item' id='logout-btn'>Logout</span>";
            var logout_btn = document.getElementById('logout-btn');
            logout_btn.addEventListener('click',function(){
                firebase.auth().signOut()
                .then(function(){
                    alert('Log out success');
                })
                .catch(function(){
                    alert('Log out error');
                })
            })
        } else {
            // It won't show any post if not login
            document.getElementById('roomName').innerHTML = "登入後即可使用聊天室";
            user_email = '';
            user_id = ''
            user_roomid_list = [];
            user_roomname_list = [];
            room_div = [];
            member_div = [];
            show = false;
            create_room_btn = document.querySelector('#create-room');
            create_room_btn.className = "nav-link disabled";
            invite_btn = document.querySelector('#invite-btn');
            invite_btn.className = "nav-link disabled";
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            document.getElementById('Chatroom-bar').innerHTML = "";
            document.getElementById('message-list').innerHTML = "";
            document.getElementById('member-list').innerHTML = "";
        }
    });
    create_room_btn = document.getElementById('create-room');
    create_room_btn.addEventListener('click',async function(){
        if(user_email == ""){
            alert("Please login first!");
        }
        else{
            var roomname = prompt("新聊天室名稱");
            if(roomname != null && roomname != ""){
                chatrooms = firebase.database().ref('chatrooms');
                var used = false;
                await chatrooms.once('value').then(function(snapshot){
                    snapshot.forEach(function(childSnapshot){
                        var roomkey = childSnapshot.val();
                        if(roomkey.name == roomname){
                            used = true;
                        }
                    })
                })
                if(used == true){
                    alert("這個聊天室名稱已被使用過，創建失敗");
                }
                else{
                    user_roomname_list.push(roomname);
                    firebase.database().ref('chatrooms').push({name : roomname}).then(async (e)=>{
                        user_roomid_list.push(e.key);
                        await firebase.database().ref("chatrooms/" + e.key + "/user").push(
                            {
                                email: user_email,
                                user_id: user_id
                            }
                        )
                        await firebase.database().ref('user_list/' + user_id + '/chatroom').push(
                            {
                                id: e.key,
                                name: roomname
                            }
                        )
                        var div_room = "<div id=\"" + user_roomid_list[user_roomid_list.length-1] + "\" onclick=\"select_room(this.id)\"" + "\"" + " class=\"room-name\">" + user_roomname_list[user_roomname_list.length-1] + "</div>\n"
                        room_div.push(div_room);
                        document.getElementById('Chatroom-bar').innerHTML = room_div.join('');
                        message_addr = firebase.database().ref('chatrooms/' + user_roomid_list[user_roomid_list.length-1] + '/message_list');
                        message_addr.on('child_added', function(e){
                            room_id = e.ref.parent.parent.key;
                            message_data = e.val();
                            if(room_id == user_roomid_list[current_room_index]){
                                var content = message_data.content;
                                content = content.replace(/</g, "&lt;").replace(/>/g, "&gt;");
                                var user = message_data.user_email;
                                if(user_email != user){
                                    var time = new Date(message_data.date);
                                    var date = 
                                    time.getFullYear()+'年'+
                                    (time.getMonth()+1)+'月'+
                                    time.getDate()+'日('+
                                    time.getHours()+':'+time.getMinutes()+
                                    ')';
                                    var div_message = "";
                                    div_message = "<div class=\"othersmessage-data\">\n<div class=\"date\">" + date + "</div>\n" + "<div class=\"username\">" + user + "</div>\n" + "<div class=\"otherscontent\">" + content + "</div>\n" + "</div>";
                                    message_div.push(div_message);
                                    document.getElementById('message-list').innerHTML = message_div.join('');
                                    document.getElementById('message-list').scrollTop = document.getElementById('message-list').scrollHeight;   
                                }
                            }
                            else{
                                if(message_data.date > Pre_update){
                                    var notify = new Notification( message_data.user_email + "傳送資訊到:" + user_roomname_list[user_roomid_list.indexOf(room_id)], {
                                        body: message_data.content
                                    });
                                    Pre_update = message_data.date;
                                    notify.onclick = function(e) {
                                        e.preventDefault();
                                        select_room(room_id);
                                    }
                                }                                     
                            }
                        })
                    })
                    alert("創建聊天室成功！")
                }
            }
            else if(roomname == ""){
                alert("聊天室名稱不能為空！")
            }
        }
    })
    invite_btn = document.getElementById('invite-btn');
    invite_btn.addEventListener('click',async function(){
        if(user_email == ""){
            alert("Please login first!");
        }
        else if(current_room_index == 0){
            alert("您現在位於公眾頻道，無法邀請別人加入！");
        }
        else{
            var invitee = prompt("請輸入email以邀請進入聊天室:" + user_roomname_list[current_room_index]);
            if(invitee == user_email){
                alert("請勿輸入您自身的email");
            }
            else if(invitee != '' && invitee != null){
                user_list = firebase.database().ref('user_list');
                var invitee_id = "";
                var already_signup = false;
                var already_inroom = false;
                await user_list.once('value').then(function(snapshot){
                    snapshot.forEach(function(childSnapshot){
                        var user = childSnapshot.val();
                        if(user.email == invitee){
                            invitee_id = childSnapshot.key;
                            already_signup = true;
                        }
                    })
                })
                if(already_signup == false){
                    alert("這個email尚未完成註冊，邀請失敗！");
                }
                else{
                    var room_data = firebase.database().ref('chatrooms/' + user_roomid_list[current_room_index] + "/user")
                    await room_data.once('value').then(function(snapshot){
                        snapshot.forEach(function(childSnapshot){
                            var user = childSnapshot.val();
                            if(user.email == invitee){
                                already_inroom = true;
                            }
                        })
                    })
                    if(already_inroom == true){
                        alert("這個用戶已在聊天室內！");
                    }
                    else{
                        await firebase.database().ref('chatrooms/' + user_roomid_list[current_room_index] + "/user").push(
                            {
                                email: invitee,
                                user_id: invitee_id 
                            }
                        )
                        await firebase.database().ref('user_list/' + invitee_id + "/chatroom").push(
                            {
                                id: user_roomid_list[current_room_index],
                                name: user_roomname_list[current_room_index]
                            }
                        )
                        var div_member = "<div class=\"member-data\">" + invitee + "</div>\n";
                        member_div.push(div_member);
                        alert("邀請" + invitee + "加入聊天室成功！")
                    }
                }
            }
            else if(invitee == ""){
                alert("受邀者email不能為空！")
            }
        }
    })
    submit_btn = document.getElementById('submit-btn');
    message_txt = document.getElementById('message');
    async function click_submit(){
        if(user_email == ""){
            alert("Please login first!");
            message_txt.value = "";
        }
        else if(message_txt.value != ""){
            var message_list = firebase.database().ref('chatrooms/' + user_roomid_list[current_room_index] + '/message_list');
            var today=new Date();
            var currentDateTime =
                today.getFullYear()+'年'+
                (today.getMonth()+1)+'月'+
                today.getDate()+'日('+
                today.getHours()+':'+today.getMinutes()+
                ')';
            await message_list.push({
                date: today.getTime(),
                content: message_txt.value,
                user_id: user_id,
                user_email: user_email 
            })
            var content = message_txt.value;
            content = content.replace(/</g, "&lt;").replace(/>/g, "&gt;");
            var user = user_email;
            var date = currentDateTime;
            if(user == user_email){
                div_message = "<div class=\"mymessage-data\">\n<div class=\"date\">" + date + "</div>\n" + "<div class=\"mycontent\">" + content + "</div>\n" + "</div>";
            }
            else{
                div_message = "<div class=\"othersmessage-data\">\n<div class=\"date\">" + date + "</div>\n" + "<div class=\"username\">" + user + "</div>\n" + "<div class=\"otherscontent\">" + content + "</div>\n" + "</div>";
            }
            message_div.push(div_message);
            document.getElementById('message-list').innerHTML = message_div.join('');
            document.getElementById('message-list').scrollTop = document.getElementById('message-list').scrollHeight;
            message_txt.value = "";
        }
    }
    document.addEventListener("keydown", logkey);
    var pre_submit = new Date();
    function logkey(e){
        var now_submit = new Date();
        if(e.key == "Enter" && now_submit.getTime() - pre_submit.getTime() > 1000){
            click_submit();
            pre_submit = now_submit;
        }
    }
    submit_btn.addEventListener('click', click_submit);

    member = document.getElementById("member");
    memberList = document.getElementById("member-list");
    member.addEventListener('click', async function(){
        if(user_email == ""){
            alert("Please login first!");
        }
        else{
            if(show){
                memberList.innerHTML = "";
                member.style.color = "white";
                show = false;
            }
            else{
                memberList.innerHTML = member_div.join('');
                member.style.color = "#2894FF";
                show = true;
            }
        }
    })
}