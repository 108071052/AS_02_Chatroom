# Software Studio 2021 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* Project Name : Midterm_Project_108071052

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|15%|Y|
|Firebase Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|20%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|

# 作品網址：https://as-02-chatroom.web.app/

## Website Detail Description
在登入後可以看到該帳號所有的聊天室（顯示在左側），點擊左側不同的聊天室，可以看到該聊天室內的訊息。  
點擊上方的New room可以創建新房間，不能創建重複名稱的聊天室。  
點擊上方的invite可以邀請其他人進入聊天室，不能邀請已在聊天室的玩家。  
玩家所在房間但非當前聊天室傳入訊息時，會顯示通知。  

# Components Description : 
Membership Mechanism : 可以做登入和註冊。  
Firebase Page : 如題所示。   
Database : 可以依權限對資料庫做讀/寫。  
RWD : 如題所示。  
Topic Key Function : 可以創建聊天室、邀請他人、傳送訊息與讀取訊息。  
Third-Party Sign In : 可以用google登入。  
Chrome Notification : 玩家所在房間但非當前聊天室傳入訊息時，會顯示通知。  
Use CSS Animation : 剛登入但資訊還未讀入時會有動畫，跳轉房間時也會有動畫。  
Security Report : 使用者傳送html code會正常出現。  
# Other Functions Description : 
1. 聊天室資訊顯示優化 : 可以看到當前所在聊天室與每則訊息的傳送時間，自己的訊息會顯示在右邊。  
2. 受到邀請時有通知，且受邀請後會馬上進入該房間。  
3. 點擊通知時會跳轉至發送通知的房間。  
4. 顯示訊息發送時間。  
5. 點及聊天室視窗右上角icon可以看到聊天室內部成員。  
